﻿using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.Common.DTO
{
    public sealed class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
