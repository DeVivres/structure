﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.Common.DTO
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
