﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.Common.DTO
{
    public sealed class TaskStateDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
