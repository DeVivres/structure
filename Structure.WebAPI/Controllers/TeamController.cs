﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Services;
using Structure.Common.DTO;
using Structure.DAL.Services;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly IService<TeamDTO> _teamService;

        public TeamController(IService<TeamDTO> teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _teamService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _teamService.Get(id);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TeamDTO value)
        {
            _teamService.Create(value);
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] TeamDTO value)
        {
            _teamService.Update(value);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _teamService.Delete(id);
            return NoContent();
        }
    }
}
