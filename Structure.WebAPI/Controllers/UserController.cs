﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Services;
using Structure.Common.DTO;
using Structure.DAL.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IService<UserDTO> _userService;

        public UserController(IService<UserDTO> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_userService.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _userService.Get(id);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserDTO value)
        {
            _userService.Create(value);
            return Ok();
        }

        [HttpPut]
        public IActionResult Put([FromBody] UserDTO value)
        {
            _userService.Update(value);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return NoContent();
        }
    }
}
