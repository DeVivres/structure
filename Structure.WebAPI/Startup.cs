using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Structure.BLL.MappingProfiles;
using Structure.BLL.Services;
using Structure.Common.DTO;
using Structure.DAL.Context;
using Structure.DAL.Interfaces;
using Structure.DAL.Services;
using Structure.DAL.UnitOfWork;

namespace Structure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TaskStateModelProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            DatabaseContext database = new DatabaseContext();

            services.AddSingleton(mapper);
            services.AddSingleton(database);
            services.AddScoped<IService<ProjectDTO>, ProjectService>();
            services.AddScoped<IService<TaskDTO>, TaskService>();
            //services.AddScoped<IService<TaskStateDTO>, TaskStateModelService>();
            services.AddScoped<IService<TeamDTO>, TeamService>();
            services.AddScoped<IService<UserDTO>, UserService>();
            services.AddAuthorization();
            services.AddControllers();

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
