﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
    public enum TaskState
    {
        Created,
        Started,
        Completed,
        Canceled
    }
}
