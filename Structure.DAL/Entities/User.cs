﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
    public sealed class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public DateTime BirthDay { get; set; }
        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty("teamId")]
        public int? TeamId { get; set; }
        public override string ToString()
        {
            return string.Format($"User Id: {Id}\n" +
                                 $"First Name: {FirstName}\n" +
                                 $"Last Name: {LastName}\n" +
                                 $"Email: {Email}\n" +
                                 $"Birth Day: {BirthDay}\n" +
                                 $"Registered At: {RegisteredAt}\n" +
                                 $"Team Id: {TeamId}\n");
        }
    }
}
