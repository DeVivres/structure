﻿using Newtonsoft.Json;
using Structure.DAL.Entities;
using System.Collections.Generic;
using System.IO;

namespace Structure.DAL.Context
{
    public class DatabaseContext
    {
        public List<Project> Projects { get; private set; }
        public List<Task> Tasks { get; private set; }
        public List<TaskState> TaskStates { get; private set; }
        public List<Team> Teams { get; private set; }
        public List<User> Users { get; private set; }

        public DatabaseContext()
        {
            Projects = GetDataFromFiles<Project>("Projects");
            Tasks = GetDataFromFiles<Task>("Tasks");
            //TaskStates = GetDataFromFiles<TaskState>("TaskState");
            Teams = GetDataFromFiles<Team>("Teams");
            Users = GetDataFromFiles<User>("Users");
        }
        public List<T> GetDataFromFiles<T>(string type)
        {
            List<T> result = null;
            using (StreamReader reader = new StreamReader($"../{type}.json"))
            {
                string json = reader.ReadToEnd();
                result = JsonConvert.DeserializeObject<List<T>>(json);
            }
            return result;
        }
    }
}
