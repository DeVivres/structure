﻿using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Structure.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly DatabaseContext _databaseContext;
        public UserRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public int CreateId()
        {
            return _databaseContext.Users.Max(s => s.Id) + 1;
        }
        public void Create(User item)
        {
            item.Id = CreateId();
            _databaseContext.Users.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _databaseContext.Users.FirstOrDefault(a => a.Id == id);
            return _databaseContext.Users.Remove(item);
        }

        public User Get(int id)
        {
            var item = _databaseContext.Users.FirstOrDefault(a => a.Id == id);
            return item;
        }

        public IEnumerable<User> GetAll()
        {
            return _databaseContext.Users;
        }

        public bool Update(User item)
        {
            var result = _databaseContext.Users.FirstOrDefault(a => a.Id == item.Id);

            if (result == null)
            {
                throw new ArgumentException("No user with such Id");
            }

            var index = _databaseContext.Users.IndexOf(result);
            _databaseContext.Users[index] = item;

            return true;
        }
    }
}
