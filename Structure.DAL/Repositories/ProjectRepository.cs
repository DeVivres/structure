﻿using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Structure.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly DatabaseContext _databaseContext;
        public ProjectRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public int CreateId()
        {
            return _databaseContext.Projects.Max(a => a.Id) + 1;
        }
        public void Create(Project item)
        {
            item.Id = CreateId();
            _databaseContext.Projects.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _databaseContext.Projects.FirstOrDefault(a => a.Id == id);
            return _databaseContext.Projects.Remove(item);
        }

        public Project Get(int id)
        {
            var item = _databaseContext.Projects.FirstOrDefault(a => a.Id == id);
            return item;
        }

        public IEnumerable<Project> GetAll()
        {
            return _databaseContext.Projects;
        }

        public bool Update(Project item)
        {
            var result = _databaseContext.Projects.FirstOrDefault(a => a.Id == item.Id);
            if(result == null)
            {
                throw new ArgumentException("No project with such Id");
            }

            var index = _databaseContext.Projects.IndexOf(result);
            _databaseContext.Projects[index] = item;
            
            return true;
        }
    }
}
