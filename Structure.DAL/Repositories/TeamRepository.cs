﻿using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Structure.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly DatabaseContext _databaseContext;
        public TeamRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public int CreateId()
        {
            return _databaseContext.Teams.Max(s => s.Id) + 1;
        }
        public void Create(Team item)
        {
            item.Id = CreateId();
            _databaseContext.Teams.Add(item);   
        }

        public bool Delete(int id)
        {
            var item = _databaseContext.Teams.FirstOrDefault(a => a.Id == id);
            return _databaseContext.Teams.Remove(item);
        }

        public Team Get(int id)
        {
            var item = _databaseContext.Teams.FirstOrDefault(a => a.Id == id);
            return item;
        }

        public IEnumerable<Team> GetAll()
        {
            return _databaseContext.Teams;
        }

        public bool Update(Team item)
        {
            var result = _databaseContext.Teams.FirstOrDefault(a => a.Id == item.Id);

            if (result == null)
            {
                throw new ArgumentException("No team with such Id");
            }

            var index = _databaseContext.Teams.IndexOf(result);
            _databaseContext.Teams[index] = item;

            return true;
        }
    }
}
