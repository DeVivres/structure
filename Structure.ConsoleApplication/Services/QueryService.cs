﻿using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.ConsoleApplication.Services
{
    class QueryService
    {
        private readonly DataCollection data;

        public QueryService()
        {
            data = new DataCollection();
        }

        public Dictionary<Project, int> GetUserTasksInProject(int userId)
        {
            return data.Projects.Where(proj => proj.AuthorId == userId)
                                .Select(proj => new { project = proj, tasksAmount = data.Tasks.Count(t => t.ProjectId == proj.Id) })
                                .ToDictionary(a => a.project, a => a.tasksAmount);
        }

        public List<Task> GetUserTasksWithLength(int userId, int maxLength)
        {
            return data.Tasks.Where(a => a.PerformerId == userId)
                             .Where(b => b.Name.Length < maxLength)
                             .ToList();
        }

        public List<(int id, string name)> GetUserFinishedTasks(int userId, int finishedAtYear)
        {
            return data.Tasks.Where(a => a.PerformerId == userId)
                             .Where(b => b.FinishedAt.Year == finishedAtYear)
                             .Select(c => (c.Id, c.Name))
                             .ToList();
        }

        public List<User> GetUsersMinAge(int minimalAge)
        {
            return data.Users.Where(a => DateTime.Now.Year - a.BirthDay.Year > minimalAge)
                             .OrderByDescending(b => b.RegisteredAt)
                             .GroupBy(c => c.TeamId)
                             .SelectMany(d => d)
                             .ToList();
        }

        public List<(User user, List<Task> tasks)> GetSortedUsersWithTasks()
        {
            var result = new List<(User, List<Task>)>();

            var request = data.Users.OrderBy(a => a.FirstName)
                                  .Select(b => new {
                                      user = b,
                                      tasks = data.Tasks
                                  .Where(c => c.PerformerId == b.Id)
                                  .OrderByDescending(d => d.Name.Length)
                                  .ToList()
                                  })
                                  .ToList();

            foreach (var obj in request)
            {
                result.Add((obj.user, obj.tasks));
            }

            return result;
        }
    }
}
