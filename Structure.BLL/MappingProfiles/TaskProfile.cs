﻿using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>().ReverseMap();
        }
    }
}
